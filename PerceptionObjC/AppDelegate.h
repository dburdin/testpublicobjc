//
//  AppDelegate.h
//  PerceptionObjC
//
//  Created by MacMini03 on 28/10/16.
//  Copyright © 2016 MacMini03. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

