//
//  main.m
//  PerceptionObjC
//
//  Created by MacMini03 on 28/10/16.
//  Copyright © 2016 MacMini03. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
